package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Printf("Currency Exchange Problem \n")

	var num_curr int
	var init_amt uint64
	var num_trans uint64
	var start, final int

	fmt.Scanf("%d", &num_curr)
	fmt.Scanf("%d %d %d %d", &init_amt, &start, &final, &num_trans)

	var vals [20][20]uint64
	for i := 0; i < num_curr; i++ {
		for j := 0; j < num_curr; j++ {
			fmt.Scanf("%d", &vals[i][j])
		}
	}

	maxforward(start, final, num_trans, vals, init_amt, num_curr)
	maxreverse(start, final, num_trans, vals, init_amt, num_curr)
}

func maxforward(src int, dst int, num_trans uint64, curr_vals [20][20]uint64, init_amt uint64, num_curr int) uint64 {
	var dest int
	source := src
	trans := append([]int{}, src)
	for i := uint64(0); i < (num_trans - 1); i++ {
		dest = rowmax(curr_vals, source)
		if i == num_trans-2 && dest == dst {
			// if last but one transaction and dest is same as actual final dest
			// then change it to second max
			dest = secondrowmax(curr_vals, source)
		}
		trans = append(trans, dest)
		source = dest
	}
	dest = dst
	trans = append(trans, dest)

	amt := calcamt(curr_vals, trans) * float64(init_amt)

	fmt.Printf("Max Forward \n")
	fmt.Printf("transactions: %v \n", trans)
	fmt.Printf("amount: %v \n", amt)
	fmt.Printf("currencies: %v \n", num_curr)

	fmt.Printf("Actual Val: %v \n", math.Mod(amt, (1000000000+7)))
	return uint64(0)
}

func maxreverse(src int, dst int, num_trans uint64, curr_vals [20][20]uint64, init_amt uint64, num_curr int) uint64 {
	var source int
	var dest = dst
	trans := append([]int{}, dest)
	for i := uint64(0); i < (num_trans - 1); i++ {
		source = colmax(curr_vals, dest)
		if i == num_trans-2 && source == src {
			// if last but one transaction and dest is same as actual final dest
			// then change it to second max
			source = secondcolmax(curr_vals, dest)
		}
		// append to the front of array
		trans = append([]int{source}, trans...)
		dest = source
	}
	source = src
	// append to the front of array
	trans = append([]int{source}, trans...)

	amt := calcamt(curr_vals, trans) * float64(init_amt)

	fmt.Printf("Max Reverse \n")
	fmt.Printf("transactions: %v \n", trans)
	fmt.Printf("amount: %v \n", amt)
	fmt.Printf("currencies: %v \n", num_curr)

	fmt.Printf("Actual Val: %v \n", math.Mod(amt, (1000000000+7)))
	return uint64(0)
}

func colmax(arr [20][20]uint64, curr int) (c int) {
	m := uint64(0)
	for i := 0; i < len(arr); i++ {
		val := arr[i][curr]
		if val > m {
			m = val
			c = i
		}
	}

	return c
}

func rowmax(arr [20][20]uint64, curr int) (c int) {
	m := uint64(0)
	for j, val := range arr[curr] {
		if val > m {
			m = val
			c = j
		}
	}

	return c
}

func calcamt(arr [20][20]uint64, trans []int) float64 {
	amt := float64(1)
	for i := 0; i < (len(trans) - 1); i++ {
		a := trans[i]
		b := trans[i+1]
		amt = amt * float64(arr[a][b])
	}
	return amt
}

func secondrowmax(arr [20][20]uint64, curr int) (c int) {
	var index int
	m := uint64(0)
	for j, val := range arr[curr] {
		if val > m {
			m = val
			index = j
		}
	}

	sm := uint64(0)
	for k, val := range arr[curr] {
		if k == index {
			continue
		}

		if val > sm {
			sm = val
			c = k
		}
	}

	return c
}

func secondcolmax(arr [20][20]uint64, curr int) (c int) {
	m := uint64(0)
	var index int
	for i := 0; i < len(arr); i++ {
		val := arr[i][curr]
		if val > m {
			m = val
			index = i
		}
	}

	sm := uint64(0)
	for k := 0; k < len(arr); k++ {
		if k == index {
			continue
		}
		val := arr[k][curr]
		if val > sm {
			sm = val
			c = k
		}
	}

	return c
}
